import { Component, OnInit } from '@angular/core';


declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard', icon: 'dashboard ', class: '' },
    { path: 'leads', title: 'Leads', icon: 'send', class: '' },
    { path: 'investors', title: 'Investors', icon: 'people', class: '' },
    { path: 'products', title: 'Products', icon: 'add_shopping_cart', class: '' },
    { path: 'service-providers', title: 'Service Providers', icon: 'donut_large', class: '' },

];
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor() { }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    isMobileMenu() {
        if (window.innerWidth > 991) {
            return false;
        }
        return true;
    };

}
