import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({

    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: AdminComponent,
                children: [
                    { path: 'dashboard', component: DashboardComponent},
                    // { path: 'goals', component: GoalsComponent, canActivate: [AppRouteGuard] },
                    // { path: 'services', component: ServicesComponent },
                    // { path: 'notifications', component: CommunicationComponent, canActivate: [AppRouteGuard] },
                    // { path: 'products', component: ProductsComponent, canActivate: [AppRouteGuard] },
                    // { path: 'goals/add-new-goal', component: AddGoalComponent, canActivate: [AppRouteGuard] },
                    // { path: 'my-profile', component: UserProfileComponent, canActivate: [AppRouteGuard] },
                    // { path: 'goals/: id', component: ViewGoalComponent, canActivate: [AppRouteGuard] },
                    // { path: 'reports', component: ReportsComponent, canActivate: [AppRouteGuard] },
                    // { path: 'reports/summary', component: SummaryComponent, canActivate: [AppRouteGuard] },
                    // { path: 'reports/cash-flow-summary', component: CashFlowComponent, canActivate: [AppRouteGuard] },
                    // { path: 'assessments', component: AssessmentsComponent, canActivate: [AppRouteGuard] },
                    // { path: 'assessments/cash-flow', component: CashFlowSummaryComponent, canActivate: [AppRouteGuard] },
                    // { path: 'appointments', component: AppointmentsComponent, canActivate: [AppRouteGuard] },
                    // { path: 'claims', component: ClaimsComponent, canActivate: [AppRouteGuard] },
                    // { path: 'claims/new-claim', component: NewClaimComponent, canActivate: [AppRouteGuard] },
                    // { path: 'reports/net-worth', component: NetWorthComponent, canActivate: [AppRouteGuard] }

                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
