import {
    Component,
    OnInit,
    HostBinding,
    HostListener,
    Input,
    ChangeDetectionStrategy
} from '@angular/core';

@Component({
    selector: 'app-notification-dropdown',
    templateUrl: './notification-dropdown.component.html',
    styleUrls: ['./notification-dropdown.component.scss']
})
export class NotificationDropdownComponent implements OnInit {

    alarm = "../../../../assets/images/alarm.png";

    @HostBinding('class')
    // tslint:disable-next-line:max-line-length
    classes = 'm-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width';

    @HostBinding('attr.m-dropdown-toggle') attrDropdownToggle = 'click';
    @HostBinding('attr.m-dropdown-persistent') attrDropdownPersisten = 'true';

    @Input() animateShake: any;
    @Input() animateBlink: any;

    constructor() {
        // animate icon shake and dot blink
        setInterval(() => {
            this.animateShake = 'm-animate-shake';
            this.animateBlink = 'm-animate-blink';
        }, 3000);
        setInterval(() => (this.animateShake = this.animateBlink = ''), 6000);
    }
    ngOnInit(): void { }
}
