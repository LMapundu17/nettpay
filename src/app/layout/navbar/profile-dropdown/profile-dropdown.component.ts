import { Component, OnInit, HostBinding, Input, ViewChild, ElementRef } from '@angular/core';
import { SafeStyle, DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AppAuthService } from 'shared/auth/app-auth.service';
@Component({
  selector: 'app-profile-dropdown',
  templateUrl: './profile-dropdown.component.html',
  styleUrls: ['./profile-dropdown.component.css']
})
export class ProfileDropdownComponent implements OnInit {
    @HostBinding('class')
    // tslint:disable-next-line:max-line-length
    classes = 'm-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light';

    @HostBinding('attr.m-dropdown-toggle') attrDropdownToggle = 'click';

    @Input() avatar: string = './assets/images/user4.jpg';
    @Input() avatarBg: SafeStyle = '';

    @ViewChild('mProfileDropdown') mProfileDropdown: ElementRef;

    constructor(
        private router: Router,
        private authService: AppAuthService,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit(): void {
        if (!this.avatarBg) {
            this.avatarBg = this.sanitizer.bypassSecurityTrustStyle('url(./assets/images/user_profile_bg.jpg)');
        }
    }

    public logout() {
        this.authService.logout(true);
    }

}
