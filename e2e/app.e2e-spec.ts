import { nettpayTemplatePage } from './app.po';

describe('nettpay App', function() {
  let page: nettpayTemplatePage;

  beforeEach(() => {
    page = new nettpayTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
